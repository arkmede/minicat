# minicat

#### 项目介绍
一款轻量化Http服务器

#### 截图说明
![输入图片说明](https://gitee.com/uploads/images/2018/0704/015123_deeae509_1200611.png "Servlet.png")

1、本项目未使用任意框架
2、本项目基于Socket实现Http服务器
3、本项目将在近期内持续更新


#### 本项目现实现功能：
1、Http服务端访问
2、Http(GET POST 多图文上传)参数装载
3、Gzip压缩
4、Cookie机制

#### 本项目将实现功能：
1、服务器配置中心
2、程序启动入口
3、外置项目加载
4、内置整合加载
5、兼容javax规范
6、jstl模板引擎


#### 运行说明：

1、新建Servlet。继承org.coody.framework.minicat.servlet.HttpServlet。实现doService方法

![输入图片说明](https://gitee.com/uploads/images/2018/0704/114513_2aba897b_1200611.png "1.png")

2、在新建的Servlet上写上@Servlet("/index.do")地址。其中/index.do即Mapping地址

![输入图片说明](https://gitee.com/uploads/images/2018/0704/114538_d094d7b7_1200611.png "2.png")

3、在org.coody.web.init.Rute方法里面调用CoreApp.init(Class<>...clazz)方法上。初始化这个Servlet。

![输入图片说明](https://gitee.com/uploads/images/2018/0704/114608_164d1c53_1200611.png "3.png")

4、运行org.coody.web.init.Rute的main方法

5、访问http://127.0.0.1/index.do即可

![输入图片说明](https://gitee.com/uploads/images/2018/0704/015123_deeae509_1200611.png "Servlet.png")

#### 相关配置：

配置类位于org.coody.framework.minicat.config.MiniCatConfig

![输入图片说明](https://gitee.com/uploads/images/2018/0704/114731_0bb54635_1200611.png "6.png")
#### 版权说明：

作者：Coody

版权：©2014-2020 Test404 All right reserved. 版权所有

反馈邮箱：644556636@qq.com

交流群号:218481849