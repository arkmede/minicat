package org.coody.framework.minicat.container;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.coody.framework.minicat.servlet.HttpServlet;
import org.coody.framework.minicat.util.AntUtil;

public class ServletContainer {

	private static final Map<String, HttpServlet> SERVLET_CONTAINER=new ConcurrentHashMap<String, HttpServlet>();
	
	
	public static HttpServlet getServlet(String path){
		HttpServlet servlet=SERVLET_CONTAINER.get(path);
		if(servlet!=null){
			return servlet;
		}
		for(String patt:SERVLET_CONTAINER.keySet()){
			if(AntUtil.isAntMatch(path, patt)){
				return SERVLET_CONTAINER.get(patt);
			}
		}
		return null;
	}
	
	public static void putServlet(String path,HttpServlet servlet){
		SERVLET_CONTAINER.put(path, servlet);
	}
}
