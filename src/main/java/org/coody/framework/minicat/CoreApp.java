package org.coody.framework.minicat;

import org.coody.framework.minicat.annotation.Servlet;
import org.coody.framework.minicat.container.ServletContainer;
import org.coody.framework.minicat.servlet.HttpServlet;
import org.coody.framework.minicat.socket.HttpService;
import org.coody.framework.minicat.util.StringUtil;

public class CoreApp {

	public static void init(Class<?>... clazzs) {
		try {
			if (StringUtil.isNullOrEmpty(clazzs)) {
				return;
			}
			// 打开端口
			HttpService.openPort();
			for (Class<?> clazz : clazzs) {
				if(!HttpServlet.class.isAssignableFrom(clazz)){
					continue;
				}
				Servlet servletFlag=clazz.getAnnotation(Servlet.class);
				if(servletFlag==null||StringUtil.isNullOrEmpty(servletFlag.value())){
					continue;
				}
				HttpServlet servlet=(HttpServlet) clazz.newInstance();
				System.out.println("注册地址:"+clazz.getName()+">>"+servletFlag.value());
				ServletContainer.putServlet(servletFlag.value(), servlet);
			}
			// 处理请求
			HttpService.doService();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
