package org.coody.framework.minicat.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.coody.framework.minicat.config.MiniCatConfig;

public class ByteUtils {


	public static byte[] readLine(InputStream inputStream) throws IOException {
		ByteArrayOutputStream swapStream = null;
		try {
			swapStream = new ByteArrayOutputStream();
			int chr = inputStream.read();
			while (chr != 10&&chr!=0) {
				swapStream.write(chr);
				chr = inputStream.read();
			}
			return swapStream.toByteArray();
		} catch (Exception e) {
			return null;
		} finally {
			swapStream.close();
		}
	}
	public static String readLineString(InputStream inputStream) {
		try {
			byte[] data =readLine(inputStream);
			if (StringUtil.isNullOrEmpty(data)) {
				return null;
			}
			return new String(data, MiniCatConfig.ENCODE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public static byte[] getBytes(InputStream inputStream,Integer length) {
		if (length < 1) {
			return null;
		}
		ByteArrayOutputStream swapStream = null;
		try {
			swapStream = new ByteArrayOutputStream();
			byte[] buff = new byte[length];
			Integer rcLength = inputStream.read(buff, 0, length);
			swapStream.write(buff, 0, rcLength);
			return swapStream.toByteArray();
		} catch (Exception e) {

			return null;
		} finally {
			try {
				swapStream.close();
			} catch (IOException e) {
			}
		}
	}


}
