package org.coody.framework.minicat.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.coody.framework.minicat.config.MiniCatConfig;
import org.coody.framework.minicat.handle.ServletHandle;

public class HttpService {

	private static ServerSocket server;

	
	public static void openPort() throws IOException {
		server = new ServerSocket(MiniCatConfig.HTTP_PORT);
	}

	public static void doService() throws IOException {
		while (true) {
			Socket socket = server.accept();
			socket.setSoTimeout(MiniCatConfig.SESSION_TIMEOUT);
			ServletHandle.doHandle(socket);
		}
	}
}